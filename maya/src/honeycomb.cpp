#include <maya/MIOStream.h>
#include <math.h>
//#include <vector.h>

#include <maya/MPxNode.h>

#include <maya/MFnTypedAttribute.h>

#include <maya/MIntArray.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MFnNurbsSurfaceData.h>
#include <maya/MPointArray.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatPointArray.h>
#include <maya/MObjectArray.h>

#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 

#include "errorMacros.h"
#include "jMayaIds.h"

#include "honeycomb.h" 

#define kTOLERANCE 0.0001
//using namespace std;

//typedef vector<MObject> OBJLIST; // use MObjectArray instead
MTypeId honeycomb::id( k_honeycomb  );

MObject honeycomb::aEdgeLength;  
MObject honeycomb::aNumUCells;  
MObject honeycomb::aNumVCells;  
 
MObject honeycomb::aOutMesh;

honeycomb::honeycomb() :m_CellsU(2), m_CellsV(2) {}
honeycomb::~honeycomb() {}

void* honeycomb::creator()
{
	return new honeycomb();
}

bool honeycomb::getInternalValueInContext( 
	const MPlug& plug,
	MDataHandle& datahandle, 	
	MDGContext & ctx  )
{
	bool isOk = true;

	if ( plug == aNumUCells ) {
		datahandle.set( m_CellsU );
		isOk = true;
	} else if ( plug == aNumVCells ) {
		datahandle.set(m_CellsV);
		isOk = true;
	} else {
		isOk = MPxNode::getInternalValueInContext( plug, datahandle , ctx );
	}
	return isOk;
}


bool honeycomb::setInternalValueInContext( 
	const MPlug& plug,
	const MDataHandle& datahandle, 	
	MDGContext & ctx )
{
	// u cells need to be even so that
	// we can wrap the grid around a cylinder
	bool isOk = true;
	if ( plug == aNumUCells ) {
		m_CellsU = datahandle.asInt();
		if ((m_CellsU % 2) == 1) m_CellsU ++;
		isOk = true;
	} else if ( plug == aNumVCells ) {
		m_CellsV = datahandle.asInt();
	} else {
		isOk = MPxNode::setInternalValueInContext( plug, datahandle, ctx );
	}
	return isOk;
}





MStatus honeycomb::compute( const MPlug& plug, MDataBlock& data )	{
	
	MStatus st;
 	MString method("honeycomb::compute");
	
	if(!( plug == aOutMesh )) return MS::kUnknownParameter;
	
	float edgeLength = data.inputValue(aEdgeLength).asFloat();
	int numU = data.inputValue(aNumUCells).asInt();
	int numV = data.inputValue(aNumVCells).asInt();
	if (numU < 2) numU = 2;
	if ((numU % 2) == 1) numU ++;
	if (numV < 1) numV = 1;


	float halfEdgeLength = edgeLength * 0.5;
	float halfCellHeight = sqrt((edgeLength * edgeLength) - (halfEdgeLength*halfEdgeLength));
	halfCellHeight *= -1;
	float cellHeight = halfCellHeight * 2.0;

	MFloatPointArray points;
	MIntArray counts;
	MIntArray connects;


	float x = 0;
	float z = 0;

	int numPairsU = numU / 2;



	//
	// start by making verts for the base edge of cells for the first row
	for (int j = 0; j < numU; ++j)
	{
		z = ((j % 2) == 0) ? 0.0 : halfCellHeight;
		points.append(MFloatPoint(x,0.0f,z));
		x += edgeLength;
	
		points.append(MFloatPoint(x,0.0f,z));
		x += halfEdgeLength;
	}

	for (int i = 0; i < numV; ++i)
	{
		// first 2 vertices of each row
		x = -halfEdgeLength;
		z = halfCellHeight + (i*cellHeight);
		points.append(MFloatPoint(x,0.0,z));
		z = (i+1)*cellHeight;
		points.append(MFloatPoint(0.0,0.0,z));

		// subsequent vertices - 4 for each pair 
		for (int j = 0; j < numPairsU; ++j)
		{
			x = ((j)*(edgeLength*3)) + edgeLength;
			z = (i+1)*cellHeight;
			points.append(MFloatPoint(x,0.0,z));
			x += halfEdgeLength;
			z += halfCellHeight;
			points.append(MFloatPoint(x,0.0,z));
			x += edgeLength;
			points.append(MFloatPoint(x,0.0,z));
			x += halfEdgeLength;
			z -= halfCellHeight;
			points.append(MFloatPoint(x,0.0,z));
		}
	}

	int numCells = numU*numV;

	// all faces are hexagons
	for (int i = 0; i < numCells; ++i)
	{
		counts.append(6);
	}



	// firstIndexOfRow represents index of lower left
	// corner of first hexagon of each row (looking down)
	int firstIndexOfRow = 0;
	for (int i = 0; i < numV; ++i)
	{

		// how many indices to increment to set the 
		// first index for the next row.
		int upperOffset = (numU*2)+1;
		if (i != 0) upperOffset = (numU*2)+2;

		int index = firstIndexOfRow;


		// work out connection info in pairs.
		for (int j = 0; j < numPairsU; ++j)
		{

			// connection info for first of pair
			connects.append(index);
			connects.append(index+1);
			connects.append(index+2);
			connects.append(index+upperOffset+1);
			connects.append(index+upperOffset);
			if (j == 0) {
				connects.append(index+upperOffset-1);
			} else {
				connects.append(index-1);
			}
			
			// connection info for second of pair
			connects.append(index+2);
			connects.append(index+3);
			connects.append(index+upperOffset+4);
			connects.append(index+upperOffset+3);
			connects.append(index+upperOffset+2);
			connects.append(index+upperOffset+1);

			index += 4;
		}

		firstIndexOfRow += upperOffset;
	}

	int	numVertices = points.length();
	int numConnects = connects.length();

	
	MFnMesh	fnM;
	
	MFnMeshData dataCreator;
	MObject outData = dataCreator.create(&st);er;

	
	MObject meshObject = fnM.create(	
		numVertices, numCells,	
		points, counts, connects,	
		outData, &st);er;

	MDataHandle hOut = data.outputValue(aOutMesh);
	hOut.set(outData);
	st = data.setClean( plug );er;

	// Make hard edges
	// meshFS.setObject(meshObject);
	// for (int i=0; i<meshFS.numEdges(); i++) meshFS.setEdgeSmoothing(i, 0);
	// meshFS.cleanupEdgeSmoothing();

	
	return MS::kSuccess;
}

MStatus honeycomb::initialize()
{
	
	MStatus st;
 	MString method("honeycomb::initialize");
	
	MFnTypedAttribute   tAttr;
	MFnNumericAttribute   nAttr;

	aEdgeLength = nAttr.create( "edgeLength", "eln", MFnNumericData::kFloat, 1.0f, &st );er
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
 	st = addAttribute(aEdgeLength);	er;
	

	aNumUCells = nAttr.create( "numUCells", "nuc", MFnNumericData::kInt, 2, &st );er
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
 	st = addAttribute(aNumUCells);	er;

	
	aNumVCells = nAttr.create( "numVCells", "nvc", MFnNumericData::kInt, 2, &st );er
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
 	st = addAttribute(aNumVCells);	er;

	aOutMesh = tAttr.create( "outMesh", "out", MFnData::kMesh, &st );er
	tAttr.setReadable(true);
	st = addAttribute(aOutMesh); er;
	
	st = attributeAffects(aEdgeLength, aOutMesh );
	st = attributeAffects(aNumUCells, aOutMesh );
	st = attributeAffects(aNumVCells, aOutMesh );

	
	return MS::kSuccess;
}
