/*
 *  honeycomb.h
 *  jtools
 *
 *  Created by jmann on 10/29/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _honeycomb
#define _honeycomb

#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MDataBlock.h>


 class honeycomb : public MPxNode
 {
 public:
 	honeycomb();
 	virtual				~honeycomb(); 
 	
 	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
 	static  void*		creator();
 	static  MStatus		initialize();
 	
 	bool getInternalValueInContext( 
 		const MPlug& plug,
 		MDataHandle& datahandle, 	
 		MDGContext & ctx  );


 	bool setInternalValueInContext( 
 		const MPlug& plug,
 		const MDataHandle& datahandle, 	
 		MDGContext & ctx );

 public:
 	
 	static  MObject     aEdgeLength;  
 	static  MObject     aNumUCells;  
 	static  MObject     aNumVCells;  


 	static  MObject     aOutMesh;      
 	static	MTypeId		id;          
 	
 private:
 	int m_CellsU;
 	int m_CellsV;
 	
 	
 };

#endif

