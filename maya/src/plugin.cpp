

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"

#include "honeycomb.h"





MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");
	
	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	

	st = plugin.registerNode( "honeycomb", honeycomb::id, honeycomb::creator, honeycomb::initialize); ert;

		MGlobal::executePythonCommand("import grist;grist.load()");

	return st;
	
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );

	st = plugin.deregisterNode( honeycomb::id );ert;

	
	
	return st;
}



